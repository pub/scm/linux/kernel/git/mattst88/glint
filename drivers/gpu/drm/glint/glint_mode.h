/*
 * Copyright 2010 Matt Turner.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER(S) OR AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * Authors: Matt Turner
 */
#ifndef __GLINT_MODE_H__
#define __GLINT_MODE_H__

#include "drmP.h"
#include "drm.h"

#define GLINT_MAX_FB_HEIGHT 4096
#define GLINT_MAX_FB_WIDTH 4096

#define GLINT_DPMS_CLEARED (-1)

#define to_glint_crtc(x) container_of(x, struct glint_crtc, base)
#define to_glint_encoder(x) container_of(x, struct glint_encoder, base)
#define to_glint_framebuffer(x) container_of(x, struct glint_framebuffer, base)

struct glint_crtc {
	struct drm_crtc			base;
	u8				lut_r[256], lut_g[256], lut_b[256];
	int				crtc_id;
	int				last_dpms;
	bool				enabled;
};

struct glint_mode_info {
	bool				mode_config_initialized;
	struct glint_crtc		*crtcs[1]; /* FIXME: how many CRTCs? */
	/* pointer to fbdev info structure */
	struct glint_fbdev		*gfbdev;
};

struct glint_encoder {
	struct drm_encoder		base;
	int				last_dpms;
};

struct glint_connector {
	struct drm_connector		base;
};

struct glint_framebuffer {
	struct drm_framebuffer		base;
};

#endif				/* __GLINT_MODE_H__ */
