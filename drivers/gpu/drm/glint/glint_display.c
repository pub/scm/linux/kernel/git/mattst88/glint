/*
 * Copyright 2010 Matt Turner.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER(S) OR AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * Authors: Matt Turner
 */
#include "drmP.h"
#include "drm.h"
#include "drm_crtc_helper.h"

#include "glint.h"
#include "glint_drv.h"

int glint_modeset_init(struct glint_device *gdev)
{
	struct drm_encoder *encoder;
	struct drm_connector *connector;
	int ret;
	int i;

	drm_mode_config_init(gdev->ddev);
	gdev->mode_info.mode_config_initialized = true;

	gdev->ddev->mode_config.max_width = GLINT_MAX_FB_WIDTH;
	gdev->ddev->mode_config.max_height = GLINT_MAX_FB_HEIGHT;

	gdev->ddev->mode_config.fb_base = gdev->mc.vram_base;

	/* allocate crtcs */
	for (i = 0; i < gdev->num_crtc; i++) {
		glint_crtc_init(gdev->ddev, i);
	}

	encoder = glint_dac_init(gdev->ddev);
	if (!encoder) {
		GLINT_ERROR("glint_dac_init failed\n");
		return -1;
	}

	connector = glint_vga_init(gdev->ddev);
	if (!connector) {
		GLINT_ERROR("glint_vga_init failed\n");
		return -1;
	}

	drm_mode_connector_attach_encoder(connector, encoder);

	ret = glint_fbdev_init(gdev);
	if (ret) {
		GLINT_ERROR("glint_fbdev_init failed\n");
		return ret;
	}

	return 0;
}

void glint_modeset_fini(struct glint_device *gdev)
{
	glint_fbdev_fini(gdev);

	if (gdev->mode_info.mode_config_initialized) {
		drm_mode_config_cleanup(gdev->ddev);
		gdev->mode_info.mode_config_initialized = false;
	}
}
