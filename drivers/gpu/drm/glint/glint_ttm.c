/*
 * Copyright (c) 2010 James Simmons
 * All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sub license,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * THE COPYRIGHT HOLDERS, AUTHORS AND/OR ITS SUPPLIERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "drmP.h"

#include <ttm/ttm_bo_api.h>
#include <ttm/ttm_bo_driver.h>
#include <ttm/ttm_placement.h>
#include <ttm/ttm_module.h>
#include <ttm/ttm_page_alloc.h>

#include "glint.h"

static struct glint_device *glint_get_gdev(struct ttm_bo_device *bdev)
{
	struct glint_mman *mman;
	struct glint_device *gdev;

	mman = container_of(bdev, struct glint_mman, bdev);
	gdev = container_of(mman, struct glint_device, mman);
	return gdev;
}

static struct ttm_backend *
glint_bo_create_ttm_backend_entry(struct ttm_bo_device *bdev)
{
	return NULL;
}

static int
glint_bo_invalidate_caches(struct ttm_bo_device *bdev, uint32_t flags)
{
	return 0;
}

static int
glint_bo_init_mem_type(struct ttm_bo_device *bdev, uint32_t type,
                         struct ttm_mem_type_manager *man)
{
	struct glint_device *gdev = glint_get_gdev(bdev);

	switch (type) {
	case TTM_PL_SYSTEM:
		/* System memory */
		man->flags = TTM_MEMTYPE_FLAG_MAPPABLE;
		man->available_caching = TTM_PL_MASK_CACHING;
		man->default_caching = TTM_PL_FLAG_CACHED;
		break;
	case TTM_PL_VRAM:
		man->flags = TTM_MEMTYPE_FLAG_FIXED |
			     TTM_MEMTYPE_FLAG_MAPPABLE;
		man->available_caching = TTM_PL_FLAG_UNCACHED |
					 TTM_PL_FLAG_WC;
		man->default_caching = TTM_PL_FLAG_WC;
		man->gpu_offset = gdev->mc.vram_base;
		break;
	default:
		DRM_ERROR("Unsupported memory type %u\n", (unsigned)type);
		return -EINVAL;
	}
	return 0;
}

static void
glint_bo_evict_flags(struct ttm_buffer_object *bo, struct ttm_placement *pl)
{
}

static int
glint_bo_move(struct ttm_buffer_object *bo, bool evict, bool intr,
		bool no_wait_reserve, bool no_wait_gpu,
		struct ttm_mem_reg *new_mem)
{
	return 0;
}

static int
glint_bo_verify_access(struct ttm_buffer_object *bo, struct file *filp)
{
	return 0;
}

static int
glint_ttm_io_mem_reserve(struct ttm_bo_device *bdev, struct ttm_mem_reg *mem)
{
	return 0;
}

static void
glint_ttm_io_mem_free(struct ttm_bo_device *bdev, struct ttm_mem_reg *mem)
{
}

static int
glint_ttm_fault_reserve_notify(struct ttm_buffer_object *bo)
{
	return 0;
}

struct ttm_bo_driver glint_bo_driver = {
	.create_ttm_backend_entry = glint_bo_create_ttm_backend_entry,
	.invalidate_caches = glint_bo_invalidate_caches,
	.init_mem_type = glint_bo_init_mem_type,
	.evict_flags = glint_bo_evict_flags,
	.move = glint_bo_move,
	.verify_access = glint_bo_verify_access,
	.fault_reserve_notify = &glint_ttm_fault_reserve_notify,
	.io_mem_reserve = &glint_ttm_io_mem_reserve,
	.io_mem_free = &glint_ttm_io_mem_free,
};
