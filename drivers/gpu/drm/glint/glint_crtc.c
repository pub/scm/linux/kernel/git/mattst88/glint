/*
 * Copyright 2000,2001 Sven Luther.
 * Copyright 2010 Matt Turner.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER(S) OR AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * Authors: Matt Turner
 *			Sven Luther
 *			Thomas Witzel
 *			Alan Hourihane
 */
#include "drmP.h"
#include "drm.h"
#include "drm_crtc_helper.h"

#include <video/pm3fb.h>

#include "glint.h"
#include "glint_drv.h"
#include "glint_mode.h"

static void glint_crtc_load_lut(struct drm_crtc *crtc)
{
	struct glint_crtc *glint_crtc = to_glint_crtc(crtc);
	struct drm_device *dev = crtc->dev;
	struct glint_device *gdev = dev->dev_private;
	int i;

	if (!crtc->enabled)
		return;

	for (i = 0; i < 256; i++) {
		WREG8(PM3RD_PaletteWriteAddress, i);
		WREG8(PM3RD_PaletteData, glint_crtc->lut_r[i]);
		WREG8(PM3RD_PaletteData, glint_crtc->lut_b[i]);
		WREG8(PM3RD_PaletteData, glint_crtc->lut_g[i]);
	}
}

static void glint_crtc_dpms(struct drm_crtc *crtc, int mode)
{
	struct glint_crtc *glint_crtc = to_glint_crtc(crtc);

	if (mode == glint_crtc->last_dpms) /* Don't do unnecesary mode changes. */
		return;

	glint_crtc->last_dpms = mode;

	switch (mode) {
	case DRM_MODE_DPMS_STANDBY:
	case DRM_MODE_DPMS_SUSPEND:
	case DRM_MODE_DPMS_OFF:
		glint_crtc->enabled = false;
		break;
	case DRM_MODE_DPMS_ON:
		glint_crtc->enabled = true;
		glint_crtc_load_lut(crtc);
		break;
	}
}

static bool glint_crtc_mode_fixup(struct drm_crtc *crtc,
				  struct drm_display_mode *mode,
				  struct drm_display_mode *adjusted_mode)
{
	return true;
}

static inline int glint_shift_bpp(unsigned bpp, int v)
{
	/*
	 * the algorithm is
	 * 	(v * bpp) / 128
	 * which simplifies to the following
	 */
	switch (bpp) {
	case 8:
		return (v >> 4);
	case 16:
		return (v >> 3);
	case 32:
		return (v >> 2);
	}
	GLINT_ERROR("Unsupported depth %u\n", bpp);
	return 0;
}

static void glint_compute_pll(unsigned long reqclock,
			      u8 *prescale,
			      u8 *feedback,
			      u8 *postscale)
{
	int f, pre, post;
	unsigned long freq;
	long freqerr = 1000;
	long currerr;

	for (f = 1; f < 256; f++) {
		for (pre = 1; pre < 256; pre++) {
			for (post = 0; post < 5; post++) {
				freq = ((2*PM3_REF_CLOCK * f) >> post) / pre;
				currerr = (reqclock > freq) ? reqclock - freq
							    : freq - reqclock;
				if (currerr < freqerr) {
					freqerr = currerr;
					*feedback = f;
					*prescale = pre;
					*postscale = post;
				}
			}
		}
	}
}

static unsigned long
PM3DAC_CalculateClock(unsigned long ReqClock,        /* In kHz units */
		      unsigned char *prescale,       /* ClkPreScale */
		      unsigned char *feedback,       /* ClkFeedBackScale */
		      unsigned char *postscale       /* ClkPostScale */
		     )
{
	unsigned long fMinVCO = 2000000; /* min fVCO is 200MHz (in 100Hz units) */
	unsigned long fMaxVCO = 6220000; /* max fVCO is 622MHz (in 100Hz units) */
	unsigned long fMinINTREF = 10000;/* min fINTREF is 1MHz (in 100Hz units) */
	unsigned long fMaxINTREF = 20000;/* max fINTREF is 2MHz (in 100Hz units) */
	unsigned long M, N, P; /* M=feedback, N=prescale, P=postscale */
	unsigned long fINTREF;
	unsigned long fVCO;
	unsigned long ActualClock;
	unsigned long RefClock = PM3_REF_CLOCK;
	long      Error;
	unsigned long LowestError = 1000000;
	unsigned int  bFoundFreq = false;
	int       cInnerLoopIterations = 0;
	int       LoopCount;
	unsigned long ClosestClock = 0;

	ReqClock*=10; /* convert into 100Hz units */
	RefClock*=10; /* convert into 100Hz units */

	for(P = 0; P <= 5; ++P)
	{
		unsigned long fVCOLowest, fVCOHighest;

		/* it is pointless going through the main loop if all values of
		 * N produce an fVCO outside the acceptable range */
		N = 1;
		M = (N * (1UL << P) * ReqClock) / (2 * RefClock);
		fVCOLowest = (2 * RefClock * M) / N;
		N = 255;
		M = (N * (1UL << P) * ReqClock) / (2 * RefClock);
		fVCOHighest = (2 * RefClock * M) / N;

		if(fVCOHighest < fMinVCO || fVCOLowest > fMaxVCO)
		{
			continue;
		}

		for(N = 1; N <= 255; ++N, ++cInnerLoopIterations)
		{
			fINTREF = RefClock / N;
			if(fINTREF < fMinINTREF || fINTREF > fMaxINTREF)
			{
				if(fINTREF > fMaxINTREF)
				{
					/* hopefully we will get into range as the prescale
					 * value increases */
					continue;
				}
				else
				{
					/* already below minimum and it will only get worse:
					 * move to the next postscale value */
					break;
				}
			}

			M = (N * (1UL << P) * ReqClock) / (2 * RefClock);
			if(M > 255)
			{
				/* M, N & P registers are only 8 bits wide */
				break;
			}

			/* we can expect rounding errors in calculating M, which
			 * will always be rounded down. So we will checkout our
			 * calculated value of M along with (M+1) */
			for(LoopCount = (M == 255) ? 1 : 2; --LoopCount >= 0; ++M)
			{
				fVCO = (2 * RefClock * M) / N;
				if(fVCO >= fMinVCO && fVCO <= fMaxVCO)
				{
					ActualClock = fVCO / (1UL << P);
					Error = ActualClock - ReqClock;
					if(Error < 0)
						Error = -Error;
					if(Error < LowestError)
					{
						bFoundFreq = true;
						LowestError = Error;
						ClosestClock = ActualClock;
						*prescale = N;
						*feedback = M;
						*postscale = P;
						if(Error == 0)
							goto Done;
					}
				}
			}
		}
	}
Done:
	if(bFoundFreq)
		ActualClock = ClosestClock;
	else
		ActualClock = 0;

	GLINT_INFO("PM3DAC_CalculateClock: Got prescale=%d, feedback=%d, postscale=%d, WantedClock = %lu00Hz ActualClock = %lu00Hz (Error %lu00Hz)\n",
		*prescale, *feedback, *postscale, ReqClock, ActualClock, LowestError);
	return(ActualClock);
}

static int glint_crtc_mode_set(struct drm_crtc *crtc,
			       struct drm_display_mode *mode,
			       struct drm_display_mode *adjusted_mode,
			       int x, int y, struct drm_framebuffer *old_fb)
{
	struct drm_device *dev = crtc->dev;
	struct glint_device *gdev = dev->dev_private;
	int bpp = crtc->fb->bits_per_pixel;

	WREG32(PM3MemBypassWriteMask, 0xffffffff);
	WREG32(PM3Aperture0, 0x00000000);
	WREG32(PM3Aperture1, 0x00000000);

#define PM3FIFODis_INPUT		(1 << 0)
#define PM3FIFODis_OUTPUT		(1 << 1)
#define PM3FIFODis_TEXTURE		(1 << 2)
	WREG32(PM3FIFODis, PM3FIFODis_INPUT | PM3FIFODis_OUTPUT | PM3FIFODis_TEXTURE);

	WREG32(PM3HTotal,  glint_shift_bpp(bpp, mode->htotal - 1));
	WREG32(PM3HsEnd,   glint_shift_bpp(bpp, mode->hsync_end - mode->hdisplay));
	WREG32(PM3HsStart, glint_shift_bpp(bpp, mode->hsync_start - mode->hdisplay));
	WREG32(PM3HbEnd,   glint_shift_bpp(bpp, mode->htotal - mode->hdisplay));
	WREG32(PM3HgEnd,   glint_shift_bpp(bpp, mode->htotal - mode->hdisplay));
	WREG32(PM3ScreenStride,  glint_shift_bpp(bpp, crtc->fb->width));
	WREG32(PM3VTotal,  mode->vtotal - 1);
	WREG32(PM3VsEnd,   mode->vsync_end - mode->vdisplay - 1);
	WREG32(PM3VsStart, mode->vsync_start - mode->vdisplay - 1);
	WREG32(PM3VbEnd,   mode->vtotal - mode->vdisplay);

	switch (bpp) {
	case 8:
		WREG32(PM3ByAperture1Mode, PM3ByApertureMode_PIXELSIZE_8BIT);
		WREG32(PM3ByAperture2Mode, PM3ByApertureMode_PIXELSIZE_8BIT);
		break;
	case 16:
#ifndef __BIG_ENDIAN
		WREG32(PM3ByAperture1Mode, PM3ByApertureMode_PIXELSIZE_16BIT);
		WREG32(PM3ByAperture2Mode, PM3ByApertureMode_PIXELSIZE_16BIT);
#else
		WREG32(PM3ByAperture1Mode, PM3ByApertureMode_PIXELSIZE_16BIT | PM3ByApertureMode_BYTESWAP_BADC);
		WREG32(PM3ByAperture2Mode, PM3ByApertureMode_PIXELSIZE_16BIT | PM3ByApertureMode_BYTESWAP_BADC);
#endif /* ! __BIG_ENDIAN */
		break;
	case 32:
#ifndef __BIG_ENDIAN
		WREG32(PM3ByAperture1Mode, PM3ByApertureMode_PIXELSIZE_32BIT);
		WREG32(PM3ByAperture2Mode, PM3ByApertureMode_PIXELSIZE_32BIT);
#else
		WREG32(PM3ByAperture1Mode, PM3ByApertureMode_PIXELSIZE_32BIT | PM3ByApertureMode_BYTESWAP_DCBA);
		WREG32(PM3ByAperture2Mode, PM3ByApertureMode_PIXELSIZE_32BIT | PM3ByApertureMode_BYTESWAP_DCBA);
#endif /* ! __BIG_ENDIAN */
		break;
	default:
		GLINT_ERROR("%s: Unsupported depth %d\n", __func__, bpp);
		return -1;
	}

	WREG32(PM3VideoControl, PM3VideoControl_ENABLE |
				PM3VideoControl_HSYNC_ACTIVE_HIGH |
				PM3VideoControl_VSYNC_ACTIVE_HIGH |
				PM3VideoControl_PIXELSIZE_32BIT);
	WREG32(PM3ScreenBase, 0);
#define PM3ChipConfig_DISABLE_SVGA	(0 << 1)
#define PM3ChipConfig_ENABLE_SVGA	(1 << 1)
	WREG32(PM3ChipConfig, RREG32(PM3ChipConfig) & ~PM3ChipConfig_ENABLE_SVGA);

	wmb();
	{
		u8 uninitialized_var(prescale);
		u8 uninitialized_var(feedback);
		u8 uninitialized_var(postscale);

		{
			u8 uninitialized_var(prescale);
			u8 uninitialized_var(feedback);
			u8 uninitialized_var(postscale);

			glint_compute_pll(adjusted_mode->clock, &prescale, &feedback, &postscale);
			GLINT_INFO("glint_compute_pll got prescale=%d feedback=%d postscale=%d\n", prescale, feedback, postscale);
		}
		PM3DAC_CalculateClock(adjusted_mode->clock, &prescale, &feedback, &postscale);
		WREG_DAC(PM3RD_DClk0PreScale, prescale);
		WREG_DAC(PM3RD_DClk0FeedbackScale, feedback);
		WREG_DAC(PM3RD_DClk0PostScale, postscale);
	}

	return 0;
}

static void glint_crtc_prepare(struct drm_crtc *crtc)
{
	struct drm_device *dev = crtc->dev;
	struct drm_crtc *crtci;

	list_for_each_entry(crtci, &dev->mode_config.crtc_list, head)
		glint_crtc_dpms(crtci, DRM_MODE_DPMS_OFF);
}

static void glint_crtc_commit(struct drm_crtc *crtc)
{
	struct drm_device *dev = crtc->dev;
	struct drm_crtc *crtci;

	list_for_each_entry(crtci, &dev->mode_config.crtc_list, head) {
		if (crtci->enabled)
			glint_crtc_dpms(crtci, DRM_MODE_DPMS_ON);
	}
}

static void glint_crtc_gamma_set(struct drm_crtc *crtc, u16 *red, u16 *green,
				  u16 *blue, uint32_t size)
{
	struct glint_crtc *glint_crtc = to_glint_crtc(crtc);
	int i;

	if (size != 256) {
		return;
	}

	for (i = 0; i < 256; i++) {
		glint_crtc->lut_r[i] = red[i];
		glint_crtc->lut_g[i] = green[i];
		glint_crtc->lut_b[i] = blue[i];
	}
	glint_crtc_load_lut(crtc);
}

static void glint_crtc_destroy(struct drm_crtc *crtc)
{
	struct glint_crtc *glint_crtc = to_glint_crtc(crtc);

	drm_crtc_cleanup(crtc);
	kfree(glint_crtc);
}

static const struct drm_crtc_funcs glint_crtc_funcs = {
	/*
	.cursor_set = glint_crtc_cursor_set,
	.cursor_move = glint_crtc_cursor_move,
	*/
	.cursor_set = NULL,
	.cursor_move = NULL,
	.gamma_set = glint_crtc_gamma_set,
	.set_config = drm_crtc_helper_set_config,
	.destroy = glint_crtc_destroy,
};

static const struct drm_crtc_helper_funcs glint_helper_funcs = {
	.dpms = glint_crtc_dpms,
	.mode_fixup = glint_crtc_mode_fixup,
	.mode_set = glint_crtc_mode_set,
	.prepare = glint_crtc_prepare,
	.commit = glint_crtc_commit,
	.load_lut = glint_crtc_load_lut,
};

void glint_crtc_init(struct drm_device *dev, int index)
{
	struct glint_device *gdev = dev->dev_private;
	struct glint_crtc *glint_crtc;
	int i;

	glint_crtc = kzalloc(sizeof(struct glint_crtc) + (GLINTFB_CONN_LIMIT * sizeof(struct drm_connector *)), GFP_KERNEL);
	if (glint_crtc == NULL)
		return;

	drm_crtc_init(dev, &glint_crtc->base, &glint_crtc_funcs);

	drm_mode_crtc_set_gamma_size(&glint_crtc->base, 256);
	glint_crtc->crtc_id = index;
	glint_crtc->last_dpms = GLINT_DPMS_CLEARED;
	gdev->mode_info.crtcs[index] = glint_crtc;

	for (i = 0; i < 256; i++) {
		glint_crtc->lut_r[i] = i;
		glint_crtc->lut_g[i] = i;
		glint_crtc->lut_b[i] = i;
	}

	drm_crtc_helper_add(&glint_crtc->base, &glint_helper_funcs);
}

/** Sets the color ramps on behalf of fbcon */
void glint_crtc_fb_gamma_set(struct drm_crtc *crtc, u16 red, u16 green,
			     u16 blue, int regno)
{
	struct glint_crtc *glint_crtc = to_glint_crtc(crtc);

	glint_crtc->lut_r[regno] = red;
	glint_crtc->lut_g[regno] = green;
	glint_crtc->lut_b[regno] = blue;
}

/** Gets the color ramps on behalf of fbcon */
void glint_crtc_fb_gamma_get(struct drm_crtc *crtc, u16 *red, u16 *green,
			     u16 *blue, int regno)
{
	struct glint_crtc *glint_crtc = to_glint_crtc(crtc);

	*red = glint_crtc->lut_r[regno];
	*green = glint_crtc->lut_g[regno];
	*blue = glint_crtc->lut_b[regno];
}
