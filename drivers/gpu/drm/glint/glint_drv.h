/*
 * Copyright 2010 Matt Turner.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER(S) OR AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * Authors: Matt Turner
 */
#ifndef __GLINT_DRV_H__
#define __GLINT_DRV_H__

#define DRIVER_AUTHOR		"Matt Turner"

#define DRIVER_NAME		"glint"
#define DRIVER_DESC		"3DLabs GLint"
#define DRIVER_DATE		"20100526"

#define DRIVER_MAJOR		1
#define DRIVER_MINOR		0
#define DRIVER_PATCHLEVEL	0

#define GLINT_INFO(fmt, arg...) DRM_INFO(DRIVER_NAME ": " fmt, ##arg)
#define GLINT_ERROR(fmt, arg...) DRM_ERROR(DRIVER_NAME ": " fmt, ##arg)

#define GLINTFB_CONN_LIMIT 4

#define RREG8(reg) ioread8(((void __iomem *)gdev->rmmio) + (reg))
#define WREG8(reg, v) iowrite8(v, ((void __iomem *)gdev->rmmio) + (reg))
#define RREG32(reg) ioread32(((void __iomem *)gdev->rmmio) + (reg))
#define WREG32(reg, v) iowrite32(v, ((void __iomem *)gdev->rmmio) + (reg))

#define WREG_DAC(reg, v)					\
	do {							\
		WREG8(PM3RD_IndexHigh, ((reg) >> 8) & 0xff);	\
		WREG8(PM3RD_IndexLow, (reg) & 0xff);		\
		WREG8(PM3RD_IndexedData, (v));			\
	} while (0)

#define MB (1024 * 1024)

#include "glint.h"

				/* glint_crtc.c */
void glint_crtc_fb_gamma_set(struct drm_crtc *crtc, u16 red, u16 green,
			     u16 blue, int regno);
void glint_crtc_fb_gamma_get(struct drm_crtc *crtc, u16 *red, u16 *green,
			     u16 *blue, int regno);
void glint_crtc_init(struct drm_device *dev, int index);

				/* glint_dac.c */
struct drm_encoder *glint_dac_init(struct drm_device *dev);

				/* glint_device.c */
int glint_device_init(struct glint_device *gdev,
		      struct drm_device *ddev,
		      struct pci_dev *pdev,
		      uint32_t flags);
void glint_device_fini(struct glint_device *gdev);

				/* glint_display.c */
int glint_modeset_init(struct glint_device *gdev);
void glint_modeset_fini(struct glint_device *gdev);

				/* glint_fbdev.c */
int glint_fbdev_init(struct glint_device *gdev);
void glint_fbdev_fini(struct glint_device *gdev);

				/* glint_framebuffer.c */
int glint_framebuffer_init(struct drm_device *dev,
			   struct glint_framebuffer *gfb,
			   struct drm_mode_fb_cmd *mode_cmd);

				/* glint_irq.c */
void glint_driver_irq_preinstall(struct drm_device *dev);
int glint_driver_irq_postinstall(struct drm_device *dev);
void glint_driver_irq_uninstall(struct drm_device *dev);
irqreturn_t glint_driver_irq_handler(DRM_IRQ_ARGS);

				/* glint_kms.c */
int glint_driver_load(struct drm_device *dev, unsigned long flags);
int glint_driver_unload(struct drm_device *dev);
extern struct drm_ioctl_desc glint_ioctls[];
extern int glint_max_ioctl;

				/* glint_ttm.c */
extern struct ttm_bo_driver glint_bo_driver;

				/* glint_vga.c */
struct drm_connector *glint_vga_init(struct drm_device *dev);

				/* init_ttm.c */
void generic_ttm_global_release(struct glint_mman *ttm);
int generic_ttm_global_init(struct glint_mman *ttm);

#endif				/* __GLINT_DRV_H__ */
