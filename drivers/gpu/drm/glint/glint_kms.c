/*
 * Copyright 2010 Matt Turner.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER(S) OR AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * Authors: Matt Turner
 */
#include "drmP.h"
#include "drm.h"

#include "glint.h"
#include "glint_drv.h"

int glint_driver_load(struct drm_device *dev, unsigned long flags)
{
	struct glint_device *gdev;
	int r;

	gdev = kzalloc(sizeof(struct glint_device), GFP_KERNEL);
	if (gdev == NULL) {
		return -ENOMEM;
	}
	dev->dev_private = (void *)gdev;

	r = glint_device_init(gdev, dev, dev->pdev, flags);
	if (r) {
		dev_err(&dev->pdev->dev, "Fatal error during GPU init\n");
		goto out;
	}

	r = glint_modeset_init(gdev);
	if (r)
		dev_err(&dev->pdev->dev, "Fatal error during modeset init\n");
out:
	if (r)
		glint_driver_unload(dev);
	return r;
}

int glint_driver_unload(struct drm_device *dev)
{
	struct glint_device *gdev = dev->dev_private;

	if (gdev == NULL)
		return 0;
	glint_modeset_fini(gdev);
	glint_device_fini(gdev);
	kfree(gdev);
	dev->dev_private = NULL;
	return 0;
}

struct drm_ioctl_desc glint_ioctls[] = {
};
int glint_max_ioctl = DRM_ARRAY_SIZE(glint_ioctls);
