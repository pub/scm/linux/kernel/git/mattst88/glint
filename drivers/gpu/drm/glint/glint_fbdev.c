/*
 * Copyright 2010 Matt Turner.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER(S) OR AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * Authors: Matt Turner
 */
#include "drmP.h"
#include "drm.h"
#include "drm_fb_helper.h"

#include <linux/fb.h>

#include "glint.h"
#include "glint_drv.h"

struct glint_fbdev {
	struct drm_fb_helper helper;
	struct glint_framebuffer gfb;
	struct list_head fbdev_list;
	struct glint_device *gdev;
};

static struct fb_ops glintfb_ops = {
	.owner = THIS_MODULE,
	.fb_check_var = drm_fb_helper_check_var,
	.fb_set_par = drm_fb_helper_set_par,
	.fb_fillrect = cfb_fillrect,
	.fb_copyarea = cfb_copyarea,
	.fb_imageblit = cfb_imageblit,
	.fb_pan_display = drm_fb_helper_pan_display,
	.fb_blank = drm_fb_helper_blank,
	.fb_setcmap = drm_fb_helper_setcmap,
};

static int glintfb_create(struct glint_fbdev *gfbdev,
			  struct drm_fb_helper_surface_size *sizes)
{
	struct glint_device *gdev = gfbdev->gdev;
	struct fb_info *info;
	struct drm_framebuffer *fb;
	struct drm_map_list *r_list, *list_t;
	struct drm_local_map *map = NULL;
	struct drm_mode_fb_cmd mode_cmd;
	struct device *device = &gdev->pdev->dev;
	int ret;

	mode_cmd.width = sizes->surface_width;
	mode_cmd.height = sizes->surface_height;
	mode_cmd.bpp = sizes->surface_bpp;
	mode_cmd.depth = sizes->surface_depth;
	mode_cmd.pitch = mode_cmd.width * ((mode_cmd.bpp + 7) / 8);

	info = framebuffer_alloc(0, device);
	if (info == NULL)
		return -ENOMEM;

	info->par = gfbdev;

	ret = glint_framebuffer_init(gdev->ddev, &gfbdev->gfb, &mode_cmd);
	if (ret)
		return ret;

	fb = &gfbdev->gfb.base;
	if (!fb) {
		GLINT_INFO("fb is NULL\n");
	}

	/* setup helper */
	gfbdev->helper.fb = fb;
	gfbdev->helper.fbdev = info;

	strcpy(info->fix.id, "glintdrmfb");

	drm_fb_helper_fill_fix(info, fb->pitch, fb->depth);

	info->flags = FBINFO_DEFAULT;
	info->fbops = &glintfb_ops;

	drm_fb_helper_fill_var(info, &gfbdev->helper, sizes->fb_width, sizes->fb_height);

	/* setup aperture base/size for vesafb takeover */
	info->apertures = alloc_apertures(1);
	if (!info->apertures) {
		ret = -ENOMEM;
		goto out_iounmap;
	}
	info->apertures->ranges[0].base = gdev->ddev->mode_config.fb_base;
	info->apertures->ranges[0].size = gdev->mc.vram_size;

	list_for_each_entry_safe(r_list, list_t, &gdev->ddev->maplist, head) {
		map = r_list->map;
		if (map->type == _DRM_FRAME_BUFFER) {
			map->handle = ioremap_nocache(map->offset, map->size);
			if (!map->handle) {
				GLINT_ERROR("fb: can't remap framebuffer\n");
				return -1;
			}
			break;
		}
	}

	info->fix.smem_start = map->offset;
	info->fix.smem_len = map->size;
	if (!info->fix.smem_len) {
		GLINT_ERROR("%s: can't count memory\n", info->fix.id);
		goto out_iounmap;
	}
	info->screen_base = map->handle;
	if (!info->screen_base) {
		GLINT_ERROR("%s: can't remap framebuffer\n", info->fix.id);
		goto out_iounmap;
	}

	info->fix.mmio_start = 0;
	info->fix.mmio_len = 0;

	ret = fb_alloc_cmap(&info->cmap, 256, 0);
	if (ret) {
		GLINT_ERROR("%s: can't allocate color map\n", info->fix.id);
		ret = -ENOMEM;
		goto out_iounmap;
	}

	DRM_INFO("fb mappable at 0x%lX\n",  info->fix.smem_start);
	DRM_INFO("vram aper at 0x%lX\n",  (unsigned long)info->fix.smem_start);
	DRM_INFO("size %lu\n", (unsigned long)info->fix.smem_len);
	DRM_INFO("fb depth is %d\n", fb->depth);
	DRM_INFO("   pitch is %d\n", fb->pitch);

	return 0;
out_iounmap:
	iounmap(map->handle);
	return ret;
}

static int glint_fb_find_or_create_single(struct drm_fb_helper *helper,
					  struct drm_fb_helper_surface_size *sizes)
{
	struct glint_fbdev *gfbdev = (struct glint_fbdev *)helper;
	int new_fb = 0;
	int ret;

	if (!helper->fb) {
		ret = glintfb_create(gfbdev, sizes);
		if (ret)
			return ret;
		new_fb = 1;
	}
	return new_fb;
}

static int glint_fbdev_destroy(struct drm_device *dev, struct glint_fbdev *gfbdev)
{
	struct fb_info *info;
	struct glint_framebuffer *gfb = &gfbdev->gfb;

	if (gfbdev->helper.fbdev) {
		info = gfbdev->helper.fbdev;

		unregister_framebuffer(info);
		if (info->cmap.len)
			fb_dealloc_cmap(&info->cmap);
		framebuffer_release(info);
	}

	drm_fb_helper_fini(&gfbdev->helper);
	drm_framebuffer_cleanup(&gfb->base);

	return 0;
}

static struct drm_fb_helper_funcs glint_fb_helper_funcs = {
	.gamma_set = glint_crtc_fb_gamma_set,
	.gamma_get = glint_crtc_fb_gamma_get,
	.fb_probe = glint_fb_find_or_create_single,
};

int glint_fbdev_init(struct glint_device *gdev)
{
	struct glint_fbdev *gfbdev;
	int bpp_sel = 32;
	int ret;

	/* select 16 bpp console on <= 32 MB cards */
	if (gdev->mc.vram_size <= (32 * MB))
		bpp_sel = 16;

	gfbdev = kzalloc(sizeof(struct glint_fbdev), GFP_KERNEL);
	if (!gfbdev)
		return -ENOMEM;

	gfbdev->gdev = gdev;
	gdev->mode_info.gfbdev = gfbdev;
	gfbdev->helper.funcs = &glint_fb_helper_funcs;

	ret = drm_fb_helper_init(gdev->ddev, &gfbdev->helper,
				 gdev->num_crtc,
				 GLINTFB_CONN_LIMIT);
	if (ret) {
		kfree(gfbdev);
		return ret;
	}
	drm_fb_helper_single_add_all_connectors(&gfbdev->helper);
	drm_fb_helper_initial_config(&gfbdev->helper, bpp_sel);

	return 0;
}

void glint_fbdev_fini(struct glint_device *gdev)
{
	if (!gdev->mode_info.gfbdev)
		return;

	glint_fbdev_destroy(gdev->ddev, gdev->mode_info.gfbdev);
	kfree(gdev->mode_info.gfbdev);
	gdev->mode_info.gfbdev = NULL;
}
