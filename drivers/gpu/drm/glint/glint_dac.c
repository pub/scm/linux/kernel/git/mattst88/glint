/*
 * Copyright 2010 Matt Turner.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER(S) OR AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * Authors: Matt Turner
 */
#include "drmP.h"
#include "drm.h"
#include "drm_crtc_helper.h"

#include <video/pm3fb.h>

#include "glint.h"
#include "glint_drv.h"
#include "glint_mode.h"

static void glint_dac_dpms(struct drm_encoder *encoder, int mode)
{
	struct drm_device *dev = encoder->dev;
	struct glint_device *gdev = dev->dev_private;
	struct glint_encoder *glint_encoder = to_glint_encoder(encoder);

	if (mode == glint_encoder->last_dpms) /* Don't do unnecesary mode changes. */
		return;

	glint_encoder->last_dpms = mode;

	switch (mode) {
	case DRM_MODE_DPMS_STANDBY:
	case DRM_MODE_DPMS_SUSPEND:
	case DRM_MODE_DPMS_OFF:
		WREG_DAC(PM3RD_DACControl, PM3RD_DACControl_DAC_POWER_OFF);
		break;
	case DRM_MODE_DPMS_ON:
		WREG_DAC(PM3RD_DACControl, PM3RD_DACControl_DAC_POWER_ON);
		break;
	}
}

static bool glint_dac_mode_fixup(struct drm_encoder *encoder,
				 struct drm_display_mode *mode,
				 struct drm_display_mode *adjusted_mode)
{
	return true;
}

static void glint_dac_mode_set(struct drm_encoder *encoder,
			       struct drm_display_mode *mode,
			       struct drm_display_mode *adjusted_mode)
{
	struct drm_device *dev = encoder->dev;
	struct glint_device *gdev = dev->dev_private;
	int bpp = encoder->crtc->fb->bits_per_pixel;
	u8 uninitialized_var(pixelsize);
	u8 uninitialized_var(colorformat);
	u8 misccontrol = PM3RD_MiscControl_HIGHCOLOR_RES_ENABLE;

	switch (bpp) {
	case 8:
		pixelsize = PM3RD_PixelSize_8_BIT_PIXELS;
		colorformat = PM3RD_ColorFormat_CI8_COLOR | PM3RD_ColorFormat_COLOR_ORDER_BLUE_LOW;
		break;
	case 16:
		pixelsize = PM3RD_PixelSize_16_BIT_PIXELS;
		colorformat = PM3RD_ColorFormat_COLOR_ORDER_BLUE_LOW | PM3RD_ColorFormat_LINEAR_COLOR_EXT_ENABLE;
		if (encoder->crtc->fb->depth == 15)
			colorformat |= PM3RD_ColorFormat_5551_FRONT_COLOR;
		else
			colorformat |= PM3RD_ColorFormat_565_FRONT_COLOR;
		misccontrol |= PM3RD_MiscControl_DIRECTCOLOR_ENABLE;
		break;
	case 32:
		pixelsize = PM3RD_PixelSize_32_BIT_PIXELS;
		colorformat = PM3RD_ColorFormat_8888_COLOR | PM3RD_ColorFormat_COLOR_ORDER_BLUE_LOW;
		misccontrol |= PM3RD_MiscControl_DIRECTCOLOR_ENABLE;
		break;
	default:
		GLINT_ERROR("%s: Unsupported depth %d\n", __func__, bpp);
		return;
	}

	WREG_DAC(PM3RD_SyncControl, 0);
	WREG_DAC(PM3RD_DACControl, PM3RD_DACControl_DAC_POWER_ON);
	WREG_DAC(PM3RD_PixelSize, pixelsize);
	WREG_DAC(PM3RD_ColorFormat, colorformat);
	WREG_DAC(PM3RD_MiscControl, misccontrol);
}

static void glint_dac_prepare(struct drm_encoder *encoder)
{
	glint_dac_dpms(encoder, DRM_MODE_DPMS_OFF);
}

static void glint_dac_commit(struct drm_encoder *encoder)
{
	/* DAC turned back on in glint_dac_mode_set() */
}

void glint_encoder_destroy(struct drm_encoder *encoder)
{
	struct glint_encoder *glint_encoder = to_glint_encoder(encoder);
	drm_encoder_cleanup(encoder);
	kfree(glint_encoder);
}

static const struct drm_encoder_helper_funcs glint_dac_helper_funcs = {
	.dpms = glint_dac_dpms,
	.mode_fixup = glint_dac_mode_fixup,
	.mode_set = glint_dac_mode_set,
	.prepare = glint_dac_prepare,
	.commit = glint_dac_commit,
};

static const struct drm_encoder_funcs glint_dac_encoder_funcs = {
	.destroy = glint_encoder_destroy,
};

struct drm_encoder *glint_dac_init(struct drm_device *dev)
{
	struct drm_encoder *encoder;
	struct glint_encoder *glint_encoder;

	glint_encoder = kzalloc(sizeof(struct glint_encoder), GFP_KERNEL);
	if (!glint_encoder)
		return NULL;

	glint_encoder->last_dpms = GLINT_DPMS_CLEARED;
	encoder = &glint_encoder->base;
	encoder->possible_crtcs = 0x1;

	drm_encoder_init(dev, encoder, &glint_dac_encoder_funcs, DRM_MODE_ENCODER_DAC);
	drm_encoder_helper_add(encoder, &glint_dac_helper_funcs);

	return encoder;
}
